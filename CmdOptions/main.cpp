#include <iostream>
#include "cmdoptions.h"

int main(int argc, const char *argv[]) {
	std::vector<std::string> arguments;
	arguments.push_back("--dimension");
	arguments.push_back("512");
	arguments.push_back("456");

	cmdoptions::OptionParser parser;
	parser.addOption(cmdoptions::Option("dimensions"), "D");
	parser.addOption(cmdoptions::Option("iterations", "1", false, false), "I");
	parser.addOption(cmdoptions::Option("bla", "1", false, false), "B");
	if (!parser.parse(arguments)) {
		std::cin.get();
		return -1;
	}

	std::vector<size_t> dims = parser["--dimensions"].getAll<size_t>();
	std::cout << dims[0] << " " << dims[1] << std::endl;
	std::cout << parser["--iterations"].get<size_t>() << std::endl;

	std::cin.get();
	return 0;
}