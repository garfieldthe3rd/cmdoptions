#pragma once

#include <map>
#include <memory>
#include <string>
#include <vector>
#include <iostream>

namespace cmdoptions {

	class MissingRequiredValueException;

	class NoSuchOptionException : public std::runtime_error {
	public:
		NoSuchOptionException(const std::string &name) :
			std::runtime_error(std::string("No option with the name ") + name + std::string(" exists")) {
		}
	};

	template < class T >
	T convertFromString(const std::string &str);

	template < class T >
	std::vector<T> split(const std::string &str, const std::string &delimiter);

	class Option {
	protected:
		std::string name;
		std::string values;
		std::string defaultValues;
		size_t minValues;
		size_t maxValues;
		size_t occurences;
		bool required;
		bool overwriteValues;

	public:
		/*template < class T >
		Option(const std::string &name, const T &defaultValue = T(), bool required = false, bool overwriteValues = false) :
			name(name), values(), defaultValues(std::to_string(defaultValue)), occurences(0), required(required), overwriteValues(overwriteValues) {
		}*/

		//template <>
		Option(const std::string &name, bool required = false, bool overwriteValues = false) :
			name(name), values(), defaultValues(""), occurences(0), required(required), overwriteValues(overwriteValues) {
		}

		Option(const std::string &name, const std::string &defaultValue, bool required = false, bool overwriteValues = false) :
			name(name), values(), defaultValues(defaultValue), occurences(0), required(required), overwriteValues(overwriteValues) {
		}

		/*template < class T >
		Option(const std::string &name, const std::vector<T> &defaultValues = std::vector<T>(), bool required = false, bool overwriteValues = false) :
			name(name), values(), defaultValues(), occurences(0), required(required), overwriteValues(overwriteValues) {
			for (auto &val : defaultValues) {
				this->defaultValues += std::to_string(val) + " ";
			}
		}*/

		//template <>
		Option(const std::string &name, const std::vector<std::string> &defaultValues, bool required = false, bool overwriteValues = false) :
			name(name), values(), defaultValues(), occurences(0), required(required), overwriteValues(overwriteValues) {
			for (const std::string &val : defaultValues) {
				this->defaultValues += val + " ";
			}
		}

		const std::string &getName() const {
			return name;
		}

		size_t getCount() const {
			return occurences;
		}

		template < class T >
		T get() const {
			if (values.size() == 0) {
				if (required) {
					throw MissingRequiredValueException(*this);
				} else {
					return convertFromString<T>(defaultValues);
				}
			} else {
				return convertFromString<T>(values);
			}
		}

		template <>
		bool get() const {
			return occurences > 0;
		}

		template < class T >
		std::vector<T> getAll() const {
			if (values.size() == 0) {
				if (required) {
					throw MissingRequiredValueException(*this);
				} else {
					return split<T>(defaultValues, " ");
				}
			} else {
				return split<T>(values, " ");
			}
		}

		void add(const std::vector<std::string> &vals) {
			if (overwriteValues) {
				values = "";
			}

			for (auto &val : vals) {
				values += val +  " ";
			}

			occurences++;
		}
	};

	class OptionParser {
	protected:
		std::vector<std::shared_ptr<Option>> options;
		std::map<std::string, std::shared_ptr<Option>> optionMap;
		std::unique_ptr<Option> arguments;

	public:
		OptionParser() : options(), optionMap(), arguments(nullptr) {
		}

		OptionParser(Option arguments) : options(), optionMap(), arguments(std::make_unique<Option>(arguments)) {
		}

		void addOption(Option option, const std::string &abbrev = std::string()) {
			options.push_back(std::make_shared<Option>(option));
			optionMap.insert(std::pair<std::string, std::shared_ptr<Option>>("--" + option.getName(), options.back()));
			if (!abbrev.empty()) {
				optionMap.insert(std::pair<std::string, std::shared_ptr<Option>>("-" + abbrev, options.back()));
			}
		}

		const Option &operator[](const std::string &name) const {
			auto option = optionMap.find(name);
			if (option != optionMap.end()) {
				return *option->second;
			} else {
				throw NoSuchOptionException(name);
			}
		}

		bool parse(const std::vector<std::string> &args) {
			std::string errors;

			auto argIter = args.begin();
			while (argIter != args.end()) {
				const std::string &currArg = *argIter;

				// TODO: check for emptyness?
				// First check if we start with a '-'; this marks options
				if (currArg.front() == '-') {
					auto optionIter = optionMap.find(currArg);
					if (optionIter == optionMap.end()) {
						errors += "Option " + currArg + " does not exist!\n";
						argIter++;
					}
					else {
						std::vector<std::string> vals;
						// Iterate until we are at the end or find another option
						// TODO: replace this with expected number of arguments!
						while (++argIter != args.end() && argIter->front() != '-') {
							std::string quotedVal = *argIter;
							// Check for quotation marks to keep arguments as one
							if (quotedVal.front() == '\"') {
								while (argIter->back() != '\"') {
									quotedVal += " " + *(argIter++);
								}
							}

							vals.push_back(quotedVal);
						}

						optionIter->second->add(vals);
					}
				}
				else {
					// TODO: this adds all arguments afterwards; is that wise?
					if (arguments != nullptr) {
						std::vector<std::string> vals;
						while (argIter != args.end()) {
							vals.push_back(*(argIter++));
						}
						arguments->add(vals);
					}
					else {
						argIter++;
					}
				}
			}
			
			if (!errors.empty()) {
				std::cerr << errors << std::endl;
				return false;
			}
			return true;
		}
	};

	class MissingRequiredValueException : public std::runtime_error {
	public:
		MissingRequiredValueException(const Option &option) :
			std::runtime_error(std::string("Missing required value for option ") + option.getName()) {
		}
	};

	template <>
	int convertFromString(const std::string &str) {
		return std::stoi(str);
	}

	template <>
	long convertFromString(const std::string &str) {
		return std::stol(str);
	}

	template <>
	long long convertFromString(const std::string &str) {
		return std::stoll(str);
	}

	template <>
	unsigned long convertFromString(const std::string &str) {
		return std::stoul(str);
	}

	template <>
	unsigned long long convertFromString(const std::string &str) {
		return std::stoull(str);
	}

	template <>
	float convertFromString(const std::string &str) {
		return std::stof(str);
	}

	template <>
	double convertFromString(const std::string &str) {
		return std::stod(str);
	}

	template <>
	std::string convertFromString(const std::string &str) {
		return str;
	}

	template < class T >
	std::vector<T> split(const std::string &str, const std::string &delimiter) {
		std::vector<T> res;

		size_t oldPos = 0;
		size_t newPos = 0;
		while ((newPos = str.find(delimiter, oldPos)) != std::string::npos) {
			res.push_back(convertFromString<T>(str.substr(oldPos, newPos - oldPos)));
			oldPos = newPos + delimiter.length();
		}

		if (oldPos < str.length()) {
			res.push_back(convertFromString<T>(str.substr(oldPos, std::string::npos)));
		}

		return res;
	}

} // namespace cmdoptions